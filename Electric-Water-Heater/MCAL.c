
#include <xc.h> 
#include "config.h"
#define _XTAL_FREQ 8000000
void pin_output(char pin, char PORT)
{ if (PORT=='A'||PORT=='a')
    TRISA&=~(1<<pin);
    else if (PORT=='B'||PORT=='b')
    TRISB&=~(1<<pin);
    else if (PORT=='C'||PORT=='c')
    TRISC&=~(1<<pin);
     else if (PORT=='D'||PORT=='d')
    TRISD&=~(1<<pin);
     else if (PORT=='E'||PORT=='e')
    TRISE&=~(1<<pin);
    }
void pin_input (char pin , char PORT)
{
    if (PORT=='A'||PORT=='a')
    TRISA|=(1<<pin);
    else if (PORT=='B'||PORT=='b')
    TRISB|=(1<<pin);
    else if (PORT=='C'||PORT=='c')
    TRISC|=(1<<pin);
     else if (PORT=='D'||PORT=='d')
    TRISD|=(1<<pin);
     else if (PORT=='E'||PORT=='e')
    TRISE|=(1<<pin);
    }   
void port_input (char PORT)
{
    if (PORT=='A'||PORT=='a')
    TRISA=255;
    else if (PORT=='B'||PORT=='b')
    TRISB=255;
    else if (PORT=='C'||PORT=='c')
    TRISC=255;
     else if (PORT=='D'||PORT=='d')
   TRISD=255;}
    
    
    
    void port_output ( char PORT)
{
    if (PORT=='A'||PORT=='a')
    TRISA=0;
    else if (PORT=='B'||PORT=='b')
    TRISB=0;
    else if (PORT=='C'||PORT=='c')
    TRISC=0;
     else if (PORT=='D'||PORT=='d')
   TRISD=0;}
    
    void pin_set ( char pin , char PORT)
    {if (PORT=='A'||PORT=='a')
   PORTA|=(1<<pin);
    else if (PORT=='B'||PORT=='b')
    PORTB|=(1<<pin);
    else if (PORT=='C'||PORT=='c')
    PORTC|=(1<<pin);
     else if (PORT=='D'||PORT=='d')
    PORTD|=(1<<pin);
     else if (PORT=='E'||PORT=='e')
    PORTE|=(1<<pin);
        
}
    void pin_clear(char pin , char PORT)
    {
if (PORT=='A'||PORT=='a')
    PORTA&=~(1<<pin);
    else if (PORT=='B'||PORT=='b')
    PORTB&=~(1<<pin);
    else if (PORT=='C'||PORT=='c')
    PORTC&=~(1<<pin);
     else if (PORT=='D'||PORT=='d')
    PORTD&=~(1<<pin);
     else if (PORT=='E'||PORT=='e')
    PORTE&=~(1<<pin);
    }
    void port_set ( char PORT)
    {
        
    if (PORT=='A'||PORT=='a')
    PORTA=255;
    else if (PORT=='B'||PORT=='b')
    PORTB=255;
    else if (PORT=='C'||PORT=='c')
    PORTC=255;
     else if (PORT=='D'||PORT=='d')
   PORTD=255;
        
     }
    void port_clear ( char pin , char PORT)
    {
         if (PORT=='A'||PORT=='a')
    PORTA=0;
    else if (PORT=='B'||PORT=='b')
    PORTB=0;
    else if (PORT=='C'||PORT=='c')
    PORTC=0;
     else if (PORT=='D'||PORT=='d')
   PORTD=0;
        
        
        
        
    }
  //
    
    
    void timer1_init()
    {
        OPTION_REG=0b00000000;
       INTCON|=(1<<7)|(1<<6)|(1<<5);
        TMR0=0;
       OSCCON|=(1<<4)|(1<<5)|(1<<6);
      //to use timer 1
       // T1CON|=(1<<5)|(1<<4);
        //TMR1=3036;
       // PIE1=(1<<0);
       // INTCON=(1<<7)|(1<<6);
       // T1CON|=(1<<0);
    }
   int  ADC_read_channel( char channel ) 
    { int result;
    TRISA=255;
    if (channel== 0)
    { ADCON0=(1<<0) ;
         ADCON1=0b10001110;}
    else if (channel==1){
    
        ADCON0=(1<<0)|(1<<3);
        ADCON1=0b10001101;
        
    
    }
    
         ADCON0|=(1<<2);
         while(ADCON0&(1<<2));
         result=(ADRESH<<8)+( ADRESL);
         result =(int )((result*5.0)/1024.0);
         return result;
    }