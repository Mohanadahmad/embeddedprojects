#include <xc.h>
#include "MCAL.h"
#include "HAL.h"
#include "config.h"

extern volatile char  time[4] , display_flag,timeset_flag;
extern volatile char system_mode,timebase_flag,button_flag;
extern volatile int buttons_counter,timebase_counter ;
extern volatile char display_state ,button_state, timeset_state,key,timebase_state;


    void reset_time()
    {int counter ;
     for (counter=0;counter<=3;counter++)
              time[counter]='0';     
    }

void set_outputs ( char mode){
pin_output(2,'c');
pin_output(5,'b');

  switch (mode) 
    {  case idle : pin_clear(2,'c');
                   pin_clear(0,'c');
                   pin_clear(5,'b');
            break;
       
      case active : pin_set(2,'c');
                    pin_clear(0,'c');

            break;
       case door_open : pin_set(5,'b');
                        pin_set(0,'c');
                        pin_clear(2,'c');
                        PORTA=255;
            break; 
      case time_set :pin_clear(2,'c');
                     pin_clear(0,'c');
                     pin_clear(5,'b');
            break;
          
    
    }}

void display_task()
{ //static char display_state =3;
    
switch (system_mode)
{ case idle : 
LCD_init();
LCD_send_string("start:* mass:PT2");
LCD_send_string_rowcolumn(1, 0,"stop:# door:POT1");
LCD_goto_row_column(2,6);
LCD_send_character(time[0]);
LCD_send_character(time[1]);
LCD_send_character(':');
LCD_send_character(time[2]);
LCD_send_character(time[3]);
display_flag=0;
break;
 case (door_open):
        LCD_clear_display();
        LCD_init();
        LCD_send_string("CLOSE THE DOOR and add food ");
        display_flag=0;
        break;


default:
LCD_init();
if(timeset_flag==1|| timeset_flag==0){
LCD_send_string_rowcolumn(2,0, "set then start");}
LCD_goto_row_column(0,5);
LCD_send_character(time[0]);
LCD_send_character(time[1]);
LCD_send_character(':');
LCD_send_character(time[2]);
LCD_send_character(time[3]);
display_flag=0;

break;


}}
void timebase_task()
{ 
    switch(timebase_state)
    {
        case 3: if (time[timebase_state]=='0') {time[timebase_state]='9'; timebase_state=2;}
        else{ --time[timebase_state]; timebase_state=3; timebase_flag=0; display_flag=1;}
        break;
        case 2 :  if (time[timebase_state]=='0') {time[timebase_state]='5'; timebase_state=1;}
        else{ --time[timebase_state]; timebase_state=3; timebase_flag=0; display_flag=1;}
        break;
        case 1:  if (time[timebase_state]=='0') {time[timebase_state]='9'; timebase_state=0;}
        else{ --time[timebase_state]; timebase_state=3; timebase_flag=0; display_flag=1;}
        break;
        case 0:   if (time[timebase_state]=='0')
        {reset_time();
        timebase_state=3; 
        timebase_flag=0; system_mode=idle; 
        set_outputs(idle); 
        display_flag=1; }
        else{ 
            --time[timebase_state];
        timebase_state=3;
        display_flag=1;}
        break;
        
        
        
        
    }
}

  
void buttons_task()
{ 
    int weight;
    
    switch (button_state)
    {case 0 :
               if (ADC_read_channel(1)==0){
         
            if (system_mode ==active||(system_mode==time_set)&&timeset_flag==2) { system_mode=time_set; set_outputs(door_open), display_flag=1 , button_state=0; }
            else {
            system_mode=door_open;
            set_outputs(door_open);
            display_flag=1; 
            reset_time();
            timeset_flag=0;
            button_state=4;
            timebase_flag=0; }}
        else {
            button_state=1;
            set_outputs(system_mode);}
    
        break;
        
           
      case 1:   
            while(key==no_key && button_flag==1)
            key = keypad_read(); 
            if (key == no_key) {button_state=0;  button_flag=0;    }
            else {button_state=2 ; }
            break ;
            
       case 2: 
               if ((key== start )&&(system_mode!= door_open)&&timeset_flag==0)
                {  
                    button_state=3;
                    key=no_key ;
                    system_mode=time_set;
                    display_flag=1;
                    
                    
                    }
                else if (key!=start&&timeset_flag==1 &&key!=cancel && key!=open_door &&  key!= close_door &&key!=no_key&& system_mode!=door_open)
                
                {
                    button_state=3;
                    system_mode=time_set;
                    
                }
                
                else if(key==start && timeset_flag==2 && system_mode!=door_open)
                {
                    weight=ADC_read_channel(0);
                    if (weight>0)
                    { system_mode=active;
                    set_outputs(active);
                    key=no_key; 
                    button_state=0; 
                   
                    display_flag=1;}
                    else {button_state=0; key=no_key;}
                
                }
                
                else if (key==cancel)
                
                {
                    if (system_mode==active)
                    {system_mode=time_set;
                    set_outputs(time_set);
                    button_state=0; button_flag=0; 
                    display_flag=1;
                    timebase_flag=0; 
                    
                    timeset_flag=2;
                    key=no_key;}
                    else
                    { system_mode=idle; 
                    set_outputs(idle);
                    reset_time();
                    button_state=0;
                    button_flag=0; 
                    timeset_state=3;
                    display_flag=1; 
                    key=no_key; 
                    timeset_flag=0;}
                
                    
                
                }
                
                
                
                
                else {
                    button_state=0;
                    button_flag=0;
                    key=no_key;
                    
                }
                break;
                
        case 3:
            if (timeset_flag==0)
            {
                button_state=1;
                key=no_key;
                time[0]='\0';
                                time[1]='\0';
                time[2]='\0';
                time[3]='\0';

                
                timeset_flag=1;
            }
            else if (timeset_flag==1)
            {
                time[timeset_state]=key;
                display_flag=1;
                if (timeset_state==0){
                
                    timeset_flag=2;
                    button_state=0;
                    key=no_key;
                    button_flag=0;
                    timeset_state=3;
                    display_flag=1;
                }
                else{timeset_state--;
                button_state=1;
                display_flag=1;
                key=no_key;}
                
            }
              break;
              
        case 4 : 
                    
                
                    if (ADC_read_channel(1) ==0)
                    {  system_mode=door_open;
                    button_state=0;
                    button_flag=0;}
                    else {    system_mode=idle; 
                    set_outputs(idle);
                    button_state=1;
                    button_flag=0;
                    display_flag=1;  }
                    
                    break;
            }

                
           
    }