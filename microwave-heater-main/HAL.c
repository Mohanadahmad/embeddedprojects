
#include <xc.h>
#include"MCAL.h"
#include"config.h"
#define _XTAL_FREQ 8000000


/******************************************************************//**
 * Function: buttons_Read()
 * 
 * /b Description:
 * 
 *This function is used to initalize keyboard module
 * PRE_CONDITION: port_input()
 * 
 * POST_CONDITION: push button is detected
 * @param [void] 
 * 
 * @return void
 * 
                                                                     * 
 *  Example:
 * 
 * @code
 * 
 *  keypad_init();
 * 
 * @endcode
 **************************************************************************/

void keypad_init() { 

    pin_output(keypad_port_column1, keypad_port_column); 
    pin_output(keypad_port_column2, keypad_port_column);
    pin_output(keypad_port_column3, keypad_port_column);
    pin_set(keypad_port_column1, keypad_port_column);
    pin_set(keypad_port_column2, keypad_port_column);
    pin_set(keypad_port_column3, keypad_port_column);
    pin_input(keypad_port_row1, keypad_port_row);
    pin_input(keypad_port_row2, keypad_port_row);
    pin_input(keypad_port_row3, keypad_port_row);
    pin_input(keypad_port_row4, keypad_port_row);
    


}


/******************************************************************//**
 * Function: buttons_Read()
 * 
 * /b Description:
 * 
 *This function is used to read push buttons pressed by user
 * PRE_CONDITION: keypad_read()
 * 
 * POST_CONDITION: push button is detected
 * @param [void] 
 * 
 * @return unsigned char
 * \b
                                                                     * 
 *  Example:
 * 
 * @code
 * 
 * button_pressed= keypad_read();
 * 
 * @endcode
 **************************************************************************/


char keypad_read() {
    char z = no_key;
    keypad_init();
    //check row1
    pin_clear(keypad_port_column1, keypad_port_column);
    if (!(keypadrowregister & (1 << keypad_port_row1))) z = '1';
    else if (!(keypadrowregister & (1 << keypad_port_row2))) z = '4';
    else if (!(keypadrowregister & (1 << keypad_port_row3))) z = '7';
    else if (!(keypadrowregister & (1 << keypad_port_row4))) z = '*';

    pin_set(keypad_port_column1, keypad_port_column);
    //checkrow2

    pin_clear(keypad_port_column2, keypad_port_column);
    __delay_ms(5);
    if (!(keypadrowregister & (1 << keypad_port_row1))) z = '2';
    else if (!(keypadrowregister & (1 << keypad_port_row2))) z = '5';
    else if (!(keypadrowregister & (1 << keypad_port_row3))) z = '8';
    else if (!(keypadrowregister & (1 << keypad_port_row4))) z = '0';
    pin_set(keypad_port_column2, keypad_port_column);
    //check row3

    pin_clear(keypad_port_column3, keypad_port_column);
       __delay_ms(5);

    if (!(keypadrowregister & (1 << keypad_port_row1))) z = '3';
    else if (!(keypadrowregister & (1 << keypad_port_row2))) z = '6';
    else if (!(keypadrowregister & (1 << keypad_port_row3))) z = '9';
    else if (!(keypadrowregister & (1 << keypad_port_row4))) z = '#';
    pin_set(keypad_port_column3, keypad_port_column);
       __delay_ms(5);
       
    
  
       
    return z;

}

/******************************************************************//**
 * Function: seven_segment_init()
 * 
 * /b Description:
 * 
 *This function is used to initialize dio pins of seven segment 
 * PRE_CONDITION: none
 * 
 * POST_CONDITION: Dio pins of seven segment is set as an output
 * @param [void] 
 * 
 * @return void
 * \b
                                                                     * 
 *  Example:
 * 
 * @code
 * 
 * seven_segment_init();
 * 
 * @endcode
 **************************************************************************/


void seven_segment_init() {
//set enable pins of seven segments as output
 
    pin_output(seven_segment1, seven_segment_enable_port);
    pin_output(seven_segment2, seven_segment_enable_port);
    pin_output(seven_segment3, seven_segment_enable_port);
    port_output(seven_segment_data_port);//set seven_segment data port as output

}
/******************************************************************//**
 * Function: seven_segment_print()
 * 
 * /b Description:
 * 
 *This function is used to print a number on a seven segment 
*
 * PRE_CONDITION: seven_segment_init()
 * 
 * POST_CONDITION: a no. is displayed on seven segment
 * @param [char] sevensegmentno 
 *  @param [char] variable
*
 * @return void
 * \b
                                                                     * 
 *  Example:
 * 
 * @code
 * 
 * seven_segment_print(sevensegment2,'2');
 * 
 * @endcode
 **************************************************************************/

void seven_segment_print(char sevensegmentno, char variable) {
    port_clear(seven_segment_enable_port);
    pin_set(sevensegmentno, seven_segment_enable_port);//set enable pin

    if (variable == '0')      PORTD = 0b00111111;
    else if (variable == '1') PORTD = 0b00000110;
    else if (variable == '2') PORTD = 0b01011011;
    else if (variable == '3') PORTD = 0b01001111;
    else if (variable == '4') PORTD = 0b01100110;
    else if (variable == '5') PORTD = 0b01101101;
    else if (variable == '6') PORTD = 0b01111101;
    else if (variable == '7') PORTD = 0b00000111;
    else if (variable == '8') PORTD = 0b01111111;
    else if (variable == '9') PORTD = 0b01101111;
    

}

/******************************************************************//**
 * Function: seven_segment_print_int()
 * 
 * /b Description:
 * 
 *This function is used to print an integer number on a two seven segments
 * PRE_CONDITION: seven_segment_init()
 * 
 * POST_CONDITION: an integer number is printed on both seven segments
 * @param [void] 
 * 
 * @return void
 * \b
                                                                     * 
 *  Example:
 * 
 * @code 
 * 
 * seven_segment_print_int(15);
 * 
 * @endcode
 **************************************************************************/
void seven_segment_print_int(int number)
{
  seven_segment_print(seven_segment2,(number/10)+'0');//print first digit of number
      __delay_ms(10);
       seven_segment_print(seven_segment3,(number%10)+'0'); //print second digit of number
       __delay_ms(10);   
    
    
}
/******************************************************************//**




void LCD_send_command(char command) {
    pin_clear(LCD_RS, LCD_control_port); //command mode 
    pin_set(LCD_enable, LCD_control_port);
   __delay_ms(50);
    port_output(LCD_data_port);
    
    LCD_data_reg = command;
    pin_clear(LCD_enable, LCD_control_port);
}




void LCD_init() { long int i ;
    pin_output(LCD_enable, LCD_control_port);
    pin_output(LCD_RS, LCD_control_port);
    port_output(LCD_data_port);
    __delay_ms(5);
    LCD_send_command(LCD_MODE);
    __delay_ms(5);
       LCD_send_command(CLEAR_DISPLAY);
    __delay_ms(5);
   // for (i=0 ; i<20000;i++);

    LCD_send_command(DISPLAY_ON_CURSOR_OFF);
   // for (i=0 ; i<200000;i++);
__delay_ms(5);
    LCD_send_command(aincrement);
  // for (i=0 ; i<200000;i++);
__delay_ms(5);
    LCD_send_command(SET_CURSOR_LOCATION);
   // for (i=0 ; i<200000;i++)


}

void LCD_clear_display() {int i;

    LCD_send_command(CLEAR_DISPLAY);
    LCD_send_command(ReturnHome);
}

void LCD_send_character(char character) {
    int i ;
    LCD_data_reg = character;
 pin_set(LCD_RS, LCD_control_port);
    pin_set(LCD_enable, LCD_control_port);
__delay_ms(50);
    pin_clear(LCD_enable, LCD_control_port);

}

void LCD_send_string(char *string) {

    int i = 0;
    while (string[i] != '\0') {
        LCD_send_character(string[i]);
        i++;

    }
}

void LCD_goto_row_column(char row, char column) {
    unsigned char address = 0;
    switch (row) {
        case 0: address = (column);
            break;
        case 1: address = column + 0x40;
            break;
        case 2: address = column + 0x10;
            break;
        case 3: address = column + 0x50;
            break;



    }

    LCD_send_command(address | (SET_CURSOR_LOCATION));


}

void LCD_send_string_rowcolumn(char row, char column, char*string) {

    LCD_goto_row_column(row, column);
    LCD_send_string(string);

}

/*void LCD_inttostring ( int integer , char *string)
{
    
 itoa (integer,string, 10);   
}
 */
