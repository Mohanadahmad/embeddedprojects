  
#ifndef config_H
#define	config_H
#define keypad_port_row 'd'
#define keypad_port_column 'b'
#define keypad_port_row1 3
#define keypad_port_row2 2
#define keypad_port_row3 1
#define keypad_port_row4 0
#define keypad_port_column1 0
#define keypad_port_column2 1
#define keypad_port_column3 2
#define keypadrowregister PORTD
#define seven_segment_enable_port 'A'
#define seven_segment_data_port 'd'
#define seven_segment1 2
#define seven_segment2 3
#define seven_segment3 4
#define seven_segment4 5
#define LCD_control_port 'e'
#define LCD_data_port  'd' 
#define LCD_enable 1
#define LCD_RS 2 
#define DISPLAY_ON_CURSOR_ON 0x0E
#define DISPLAY_ON_CURSOR_OFF 0x0C
#define CLEAR_DISPLAY 0x01
#define SET_CURSOR_LOCATION 0x80
#define ReturnHome 0x02
#define LCD_MODE    0x38
#define  DDRAM  0x80 
#define aincrement 0x06
#define  LCD_data_reg  PORTD 
#define idle 1
#define start '*'
#define cancel '#'
#define door_open 2
#define time_set 4
#define active 3
#define no_key 'i'
#define open_door 'o'
#define close_door 'c'
 





#endif	/* XC_HEADER_TEMPLATE_H */

