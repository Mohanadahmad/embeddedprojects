
#ifndef HAL_H
#define	HAL_H
#include <xc.h>   
void keypad_init ();
char keypad_read();
void seven_segment_init();
void seven_segment_print (char sevensegmentno, char variable);
void LCD_init () ;
void LCD_send_command( char command);
void LCD_clear_display ();
void LCD_send_character( char character);
void LCD_send_string (char *string);
void LCD_goto_row_column (char row , char column );
void LCD_send_string_rowcolumn(char row, char column , char*string);
void LCD_inttostring ( int integer , char *string);
void delay () ;


#endif